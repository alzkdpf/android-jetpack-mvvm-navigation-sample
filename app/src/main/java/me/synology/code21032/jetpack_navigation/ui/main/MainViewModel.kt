package me.synology.code21032.jetpack_navigation.ui.main

import android.arch.lifecycle.ViewModel
import android.util.Log
import android.view.View
import androidx.navigation.Navigation
import me.synology.code21032.jetpack_navigation.R

class MainViewModel : ViewModel() {

    // text message
    var textMessage: String? = null

    // on click
    fun goSecondFragment(): View.OnClickListener
    {
        Log.d("test","test")
        return Navigation.createNavigateOnClickListener(R.id.action_main_to_second)
    }
}
