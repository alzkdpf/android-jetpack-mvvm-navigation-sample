package me.synology.code21032.jetpack_navigation.ui.main

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.main_fragment.*
import me.synology.code21032.jetpack_navigation.R
import me.synology.code21032.jetpack_navigation.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binder: MainFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        //binding 
        binder = DataBindingUtil.inflate(inflater,
                R.layout.main_fragment, container, false)
        return binder.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //set view model
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.textMessage = "go Second"
        binder.prs = viewModel
    }

}
